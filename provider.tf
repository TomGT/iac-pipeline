
# variable "KEY_SERVICE_GCP" {
#   type = string
# }

provider "google" {
  project     = "iac-pipeline-408616"
  region      = "europe-west"
  zone        = "europe-west9-a"
#   credentials = file(var.KEY_SERVICE_GCP)
    credentials = file("key_service_gcp.json")
}

