
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.51.0"
    }
  }

    backend "http" {}
}


### Config VM
resource "google_compute_instance" "gitlab-vm" {
  boot_disk {
    auto_delete = true
    device_name = "gitlab-vm"

    initialize_params {
      image = "projects/debian-cloud/global/images/debian-11-bullseye-v20231212"
      size  = 10
      type  = "pd-standard"
    }

    mode = "READ_WRITE"
  }

  network_interface {
    subnetwork = google_compute_network.gitlab-network.self_link
  }

  machine_type = "e2-micro"
  name         = "gitlab-vm"
  zone         = "europe-west9-a"
}

### Config VPC
resource "google_compute_network" "gitlab-network" {
  auto_create_subnetworks = true
  name                    = "gitlab-network"
  project                 = "iac-pipeline-408616"
  routing_mode            = "REGIONAL"
}